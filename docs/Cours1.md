---
outline: deep
---
# Cours 1
# Planification de la semaine

- 3 cours (Aujourd'hui, mercredi et vendredi-qui-se-prend-pour-un-lundi)

# Planification
<!-- no toc -->
- [Présentation et plan de cours](#présentations)
- [Mise en garde](#mise-en-garde)
- [MAJ VS2022](#mise-à-jour-de-visual-studio)
- [Décrassage de l'été](#décrassage-de-lété)

# Présentations

- Présentation personnelle
- Présentation du cours : https://gitlab.com/progavance/contenu/-/blob/main/420-14C-FX%20-%20A24%20-%20JeaBou.pdf
- Votre présentation : https://docs.google.com/forms/d/171zUTbWONAW9QDtNh7QygAU7AV3qbzfeHbcvAwBb5ZY

# Mise en garde

- Le cerveau est comme un muscle, il se développe à l'usage ... et c'est pas tant le fun de le développer.
- Métaphore de la forêt. Il faut ouvrir les sentiers et les entretenir. 
- Par les actions qu'on faît, on trace les sentiers et on grandit ... à condition que ce soit nous qui réalise l'action!
- Le corollaire, c'est de se méfier de ceux qui proposent de faire à notre place. Si quelqu'un fait à ma place, alors c'est lui qui apprend et qui grandit, tandis que je stagne.
- Si vous voulez devenir plus fort, ça ne donne rien de demander à quelqu'un d'aller au gym à notre place.
- Les IAG
  - Des outils de productivité.
  - Néfaste pour l'apprentissage. Ils empêchent de tracer les sentiers de la connaissance.
  - Les travaux pratiques sont le coeur de ce cours. Si vous vous empêchez d'apprendre en utilisant les IAG, il sera difficile d'être performant aux examens.

# Mise à jour de Visual Studio

Si ce n'est pas déjà fait, installer Visual Studio 2022
1.	https://visualstudio.microsoft.com/fr/downloads/
2.	Télécharger et installer VS 2022

Une fois que vous avez VS2022, à partir de VS :
1.	Aller dans Aide -­> Rechercher les mises à jour
2.	Lancer Visual Studio Installer
3.	(Optionnel) Faire la mise à jour de Visual Studio Installer si c'est nécessaire
4.	Faire la mise à jour de Visual Studio

Se rendre à https://dotnet.microsoft.com/download/dotnet
1.	Télécharger et installer le SDK de la version « Latest » de .Net 8.0

Lancer Visual Studio
1. Test #1 
   1. Continuer sans code
   2. Aide –> À propos de Microsoft Visual Studio
   3. Vous devriez avoir : 
      1. VS Version 17.11.0
      2. Microsoft .NET Framework Version 4.8.xxxx
2. Test #2
   1. Nouveau –> Projet
   2. Choisir une Application C# (WPF ou Forms peu importe)
   3. Faire suivant deux fois 
   4. Vous devriez être capable de prendre .NET 8.0 comme infrastructure

# Décrassage de l'été

- FizzBuzz
1. Meilleure solution pour l'humain (lisibilité de code)
2. Meilleure solution pour l'ordinateur (vitesse d'exécution, nombre d'opérations)
3. Pire solution