import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  base:"/docs/",
  title: "ProgAvance",
  description: "Notes de cours pour ProgAvance",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Accueil', link: '/' }
    ],

    sidebar: [
      {
        
      }
    ],

  }
})
