---
outline: deep
---

# Cours 14

# Objectifs
- Sécurité et authentification
- Persistance des données sans bd et sans douleur
- Présentation Tp2
  
# Planification
<!-- no toc -->
- [Sécurité](#sécurité)
- [Persistance données](#persistance-des-données)
- [Présentation Tp2](#présentation-tp2)
- [Code du cours](#code-du-cours)


# Sécurité 
- Félicitations vous héritez d'un commerce!
- Vous devez changer une porte arrière qui donne accès à votre commerce pour les employés et les livreurs.
  1. Une porte à clé
  2. Une porte avec un code à numéro
  3. Une porte avec un lecteur de carte
  4. Une porte qui s'ouvre seulement de l'intérieur
- Balance entre utilisabilité et sécurité.

### Sécurité pour authentification
La règle #1 c'est de ne pas coder une authentification sécurisée. On va le faire maintenant pour la seule fois de votre vie j'espère, à des fins de compréhension et de vulgarisation des principes de base de sécurité.


### Le point de départ de l'authentification : les fonctions de hachage de mot de passe

- Hachage vs Encryption : Le hachage est unidirectionnel (mot de passe -> hash) tandis que l'encryption est bidirectionnel (mot de passe <-> hash)

- Il faut que les mots de passe ne soient jamais accessibles en "clair" et ce pour personne. Alors, les mots de passes ne doivent pas être conservés tel quel dans une base de données.
- On utilise une fonction de hachage pour convertir le mot de passe en un hash(le résultat d'une fonction de hachage).
- Ainsi, ce qui est conservé dans la base de données c'est le hash et non pas le mot de passe.
- L'authentification devient alors
  1. Saisie des infos de l'utilisateur
  2. Passage du mot de passe dans une fonction de hachage
  3. Comparaison du hash résultat en #2 avec le contenu de la base de données.

### L'attaque classique : la force brute

- Programme automatisé qui essaie rapidement des combinaisons de nom d'utilisateur et mot de passe
- Exemple classique d'une cadenas à 3 chiffres
  - 1-1-1
  - 1-1-2
  - 1-1-3
  - ...
- Pas besoin d'avoir le bon mot de passe, simplement que le hash résulant soit identique au hash dans la base de données.
- Comme nous l'avons mentionné dans le dernier cours sur les structures associatives (table de hachage), une fonction de hachage n'est pas parfaite et peut causer deux hash identiques pour deux mots de passe différents.
- Les fonctions de hachage qui vont trop vite ou qui n'ont pas une bonne distribution des hash (qui causent beaucoup de collisions) sont susceptibles aux attaques par force brute.
- Défenses
  - Nombre d'essai maximal par laps de temps (tant que la base de données n'est pas compromise)
  - Fonction de hachage lente

### L'attaque quand une base de données est compromise : la table arc-en-ciel (rainbow table)

- La question n'est pas de savoir si votre base de données sera compromise, elle est plutôt quand elle le sera!
- Une fois que la base de données est compromise, l'attaque par table arc-en-ciel devient possible.
- L'attaque consiste à utiliser une liste des mots de passes les plus communs avec les fonctions de hachage les plus communes et en peu de temps il sera évident de savoir quelle est la fonction de hachage utilisée.
- Une fois que la fonction de hachage est connue, il est facile de trouver tous les mots de passes qui ne sont pas secure (courte chaîne de caractères, pas généré par des gestionnaires de mots de passes, ...).
- Défense
  - Salage

### Salage (salting)

- Ajout d'une string aléatoire à la fin des mots de passe avant de les passer dans la fonction de hachage.
- Permet de rendre tous les mots de passes secures (même qwerty et admin123).
- Deux conditions essentielles au salage
  - Immuable pour un usager (toujours pareil dans le temps)
  - Différent pour chaque usager (sinon le salt pourrait être retrouvé par brute force et ça le rendrait inutile)
- Ainsi, deux utilisateurs avec le même mot de passe ont un hash résultant différents et l'attaque par table arc-en-ciel est impossible.

### Une question demeure : Quel est la bonne fonction de hachage ?

- Plusieurs existent : MD, SHA, ...
- En ce moment celui recommandé est le Argon2id

- La référence : https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html

### En code

1. Ajouter le nuget Konscious.Security.Cryptography.Argon2 dans votre projet
2. Créer une classe static contenant ce code : 

```c#
using System.Security.Cryptography;
using Konscious.Security.Cryptography;
    
//Générateur de nombre aléatoire pour le salt
private static RandomNumberGenerator rng = RandomNumberGenerator.Create();

/// <summary>
/// Méthode qui permet de vérifier la correspondance entre un mot de passe,
/// un salt et un hash.
/// </summary>
/// <param name="mdp">Le mot de passe</param>
/// <param name="salt">Le salt</param>
/// <param name="hash">Le hash</param>
/// <returns>Vrai si le mot de passe correspond au hash.</returns>
public static bool VerifierMotDePasse(string mdp, byte[] salt, byte[] hash)
   {
        return hash.SequenceEqual(HashUnMotDePasse(mdp, salt));
   }

/// <summary>
/// Méthode qui permet de hasher un mot de passe. Il est obligatoire
/// d'utiliser un salt.
/// </summary>
/// <param name="mdp">Le mot de passe</param>
/// <param name="salt">Le salt</param>
/// <returns>Le hash</returns>
public static byte[] HashUnMotDePasse(string mdp, byte[] salt)
   {
       Argon2id argon2 = new Argon2id(Encoding.UTF8.GetBytes(mdp))
       {
           Salt = salt,
           DegreeOfParallelism = 8,
           Iterations = 4,
           MemorySize = 1024 * 1024
       };
       return argon2.GetBytes(16);
    }

/// <summary>
/// Méthode qui permet de créer un salt pour un nouvel utilisateur.
/// </summary>
/// <returns>Un salt aléatoire pour un utilisateur</returns>
public static byte[] CreerNouveauSalt()
   {
       byte[] buffer = new byte[16];
       rng.GetBytes(buffer);
       return buffer;
   }

```
3. Une classe Utilisateur avec minimalement ceci :

```c#
public string nomUsager { get; set; }
public byte[] mdp { get; set; }

public Utilisateur(string pNomUsager, byte[] pMdp)
{
    nomUsager = pNomUsager;
    mdp = pMdp;
}
```

4. Deux dictionnaires, un pour conserver les utilisateurs et un pour les salts.

```c#
Dictionary<string, Utilisateur> lesUtilisateurs;
Dictionary<string, byte[]> lesSalts;
```

# Persistance des données

1. Ajouter le nuget Newtonsoft.Json dans votre projet
2. Se créer une méthode pour charger les données

```c#
 public void ChargerDonnees()
 {
     if (File.Exists("monFichier.uneExtensionfunky"))
     {
         using (StreamReader sr = new StreamReader("monFichier.uneExtensionfunky"))
             maStructureDeDonnes = JsonConvert.DeserializeObject<leTypeDeMaStructure>(sr.ReadToEnd());
     }
 }
 ```

 3. Appeler la méthode ChargerDonnees au démarrage de votre app
 4. Se créer une méthode pour sauvegarder les données

```c#
 private void SauvegarderDonnees()
 {
     using (StreamWriter sw = new StreamWriter("monFichier.uneExtensionfunky"))
         sw.Write(JsonConvert.SerializeObject(maStructureDeDonnes));
 }
 ```

 5. Etre heureux de ne pas avoir fait un million de string.machin.
 6. Etre encore plus heureux de ne pas avoir fait des scripts SQL à bras.

# Présentation Tp2

Énoncé : https://gitlab.com/progavance/contenu/-/blob/main/Tp2.pdf?ref_type=heads
Code de départ : https://gitlab.com/JpBoucher/tp2a24-djai



# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/AuthentificationA24.zip?ref_type=heads