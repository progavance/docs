---
outline: deep
---

# Cours 7

# Objectifs
- Consolider l'héritage
  
# Planification
<!-- no toc -->
- [Les deux prochaines semaines](#les-deux-prochaines-semaines)
- [Activité révision](#un-projet-de-petite-ampleur)
- [Code fait en classe](#code-fait-en-classe)

# Les deux prochaines semaines

- Question de voir venir
  - Aujourd'hui : Activité révision
    - Max 60 min
    - 120 min pour le Tp1
  - Mercredi 18 : Activité révision suite
    - Max 60 min
    - 120 min pour le Tp1
  - Lundi 23 : Correction DSQ + Pratique examen 1
    - Correction des trois parties du DSQ (Cours 3, 4 et 5)
    - Pratique avec un ancien examen
  - Mercredi 25 : Examen 1
    - Examen papier
    - Durée de 120 min officiellement
    - Seulement sur Héritage et interface
    - 4 questions 30 min (45 min)
    - Une question de modélisation (classe et attributs)
    - Une question de modélisation (méthodes)
    - Deux questions de code de méthodes (polymorphisme et utilisation de classes hérités)
  - Un mot sur la rétroaction/correction

# Un projet de petite ampleur

- Amazon! 
- Né en 1994 comme un bookstore
- Diversification de produits
- Crash dans le temps des fêtes
- On achète un bazillion de serveur
- Ça nous sert juste une semaine par année
- AWS est né
- AWS = environ 50% du cloud public
- AWS 2023 Q2 = 70% des bénéfices de Amazon

### Structure de classe

- Produits (isbn, nom, desc, prix, vendeur, ...)
  - Électronique
  - Livres
  - Films
  - Jeux
  - Nourriture !!
- Clients (user, mdp)
  - Client
  - Client Prime
  - Vendeur
  - Vendeur Prime
  - Client AWS
- Commande (produits, client, adresse livraison, adresse facturation)


# Code fait en classe

https://gitlab.com/progavance/contenu/-/blob/main/AmazonA24.zip?ref_type=heads