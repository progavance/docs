---
outline: deep
---

# Cours 25

# Objectifs
- Création d'une structure de donnée personnalisée
  
# Planification
<!-- no toc -->
- [Deux dernières semaines](#deux-dernières-semaines)
- [Liste liée ](#liste-liée-meilleure-que-vs)
- [Code du cours](#code-du-cours)

# Deux dernières semaines
- Lundi 25 novembre : Liste liée meilleure que VS (60 min max)
- Mercredi 27 novembre : Arbre binaire (60 min max)
- Lundi 2 décembre : Temps pour le Tp3
- Mercredi 5 décembre : Arbre binaire auto-balancé (60 min max) + remise Tp3 22h
  
# Liste liée meilleure que VS

- Quelles sont les faiblesses de la liste liée de VS ?
- Peut-on créer une nouvelle structure qui palliera à ces faiblesses ?
- Peut-on aussi améliorer la structure pour la rendre encore plus puissante, notamment en automatisant certains comportements ?
- Cette nouvelle structure est en quelque sorte un hybride entre les structures linéaires et les structures arborescentes.
- Voici la liste liée triée super géniale

### En code

- Code de départ : https://gitlab.com/progavance/contenu/-/blob/main/ListeLieeMeilleureQueVS.zip?ref_type=heads
- Quelle est la structure ?
- Ajouter un élément
- Trouver un élément
- Supprimer un élément

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/ListeLieeMeilleureQueVSFin.zip?ref_type=heads