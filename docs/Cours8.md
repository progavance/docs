---
outline: deep
---

# Cours 8

# Objectifs
- Consolider l'héritage
  
# Planification
<!-- no toc -->
- [Activité révision](#amazon)
- [Code fait en classe](#code-fait-en-classe)

# Amazon

- Se Connecter
  - Qui peut se connecter ? 
  - De quelle façon ?
- Affichage
  - Qui peut s'afficher ?
  - De quelle façon ?
- CalculerPrix
  - Qui peut calculer son prix ?
  - Quel est le calcul ?

- Éco-Frais
  - Un montant fixe à ajouter sur certains types de produits
- Frais de douanes
  - Un % de frais pour certains types de produits. Le % peut varier d'un type à l'autre, et d'un pays à l'autre.
- Calcul de taxes
  - Selon le type de produits et l'adresse de livraison


# Code fait en classe

https://gitlab.com/progavance/contenu/-/blob/main/AmazonA24%20partie%202.zip?ref_type=heads