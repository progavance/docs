---
outline: deep
---

# Cours 11

# Objectifs
- Comprendre le concept de structure de données
- Remplir votre coffre à outils
  
# Planification
<!-- no toc -->
- [Prochaines semaines](#deux-prochaines-semaines)
- [Échauffement](#échauffement)
- [Structures de données](#structures-de-données)
- [Exercices](#exercices)
- [Code du cours](#code-du-cours)

# Deux prochaines semaines

- Aujourd'hui - Structure linéaire (vecteur, liste, liste liée)
- Mercredi 2 octobre - Structure linéaire (pile, file)
- Vendredi 4 octobre vers 17h - Notes Tp1 + Ex1 sur Omnivox
- Lundi 7 octobre - Structure associative (dictionnaire)
- Mercredi 9 octobre - Sécurité + Présentation Tp2
- Relâche!

# Échauffement

Sur les tableaux blancs en équipe, coder les méthodes Inserer et Suivant de la classe Utilitaires

```c#
/// <summary>
/// La classe mère qui est héritée par chacun des types de ContenuVisuel
/// que Netflix contient
/// </summary>
public abstract class ContenuVisuel
{
    private string _nom = default!;

    public string Nom { get => _nom; set => _nom = value; }
    [...]
}

public static class Utilitaires
{
    /// <summary>
    /// Représente une catégorie de la page d'accueil de Netflix
    /// </summary>
    private List<ContenuVisuel> laCategorie;

    /// <summary>
    /// Permet d'insérer un contenu visuel dans laCatégorie.
    /// La méthode insère pContenuAAjouter après pNom.
    /// </summary>
    /// <param name="pNom">Le nom du contenu qui permet de savoir où insérer</param>
    /// <param name="pContenuAAjouter">Le contenu a insérer</param>
    public void Inserer(string pNom, ContenuVisuel pContenuAAjouter)
    {
        
    }

    /// <summary>
    /// Permet de retourner le contenu Suivant de l'élément
    /// dont le nom est passé en paramètre
    /// </summary>
    /// <param name="pNom">-Le nom du contenu pour lequel on veut obtenir le contenu suivant</param>
    /// <returns>Le contenu suivant de pNom</returns>
    public ContenuVisuel Suivant(string pNom)
    {

    }
}

```

# Structures de données

- Le bloc 1 sur l'héritage et l'interface concernait la modélisation des données.
- Les blocs 2 et 3 seront sur les structures de données.

- Une structure de données c'est un ensemble de données ordonnée de façon logique.
- Ce n'est pas un nouvel objet, mais un ensemble d'objets. Peut être vu comme la dynamique du programme (type d'opérations, ajout, retrait, recherche, ...).
- Il existe plusieurs structures de données.
- Elles ont chacune leurs forces et leurs faiblesses.
- Le choix de la bonne structure est très important.
- Comment organiser mes objets ?  

### Un peu de neuropsy

- La mémoire de travail
  - Quel est ton nom ?
  - Limité et rapide
- La mémoire à long terme
  - Qu'est-ce que tu as mangé samedi dernier ?
  - Grosse et lente

Suggestion de lecture : https://fxg.koha.collecto.ca/cgi-bin/koha/opac-detail.pl?biblionumber=147341

### La pile et le tas

- La pile (ou le stack)
  - La mémoire de travail
  - Un peu comme de la RAM, rapide mais petit
  - Stack overflow
  - Seulement les types primitifs (int, bool, double, ...)
- Le tas (ou le heap)
  - La mémoire à long terme
  - Un peu comme le disque dur, lent mais gros
  - Pointeurs dans la pile, données dans le tas

### Structures de données linéaire simple

- Vecteur
  - Allocation de mémoire se fait individuellement et manuellement
  - Allocation contiguë
- Liste (list<>)
  - Allocation se fait automatiquement (double la taille)
  - Allocation par blocs
  - Toute modification qui n'est pas à la fin est couteuse
- Liste liée (linkedlist<>)
  - Les éléments sont liés
  - Liste doublement chaînée
  - Ajout et supression rapide
  - Pas d'accès direct (via index) aux éléments

![Structures linéaires simple](/images/strucLin.png)    

### Assez de blabla, codons

- Qui a le portable le plus puissant ?
  
- Ajout
- Insertion
- Accès par index

# Exercices

1.	Écrivez une méthode Joindre qui reçoit deux listes liées triées d’entiers et qui retourne une liste liée triée contenant tous les entiers des deux listes.
2.	Écrivez une méthode NombreValMin qui reçoit une liste liée d’entier en paramètre et qui trouve la valeur minimale contenue dans la liste et retourne le nombre de fois qu’elle est contenue dans la liste.
3.	Écrivez une méthode Split (sans utiliser la fonction Split de VS 😊) qui reçoit en paramètre une string ainsi qu’un char sur lequel faire le split.  Cette fonction retourne une list de string.  Par exemple si j’appelle cette méthode avec la liste « J’aime les patates » et le caractère « a », je veux que la fonction retourne la list « J’ », «ime les p », « t », « tes ».
4.	Écrivez une méthode Podium qui reçoit une liste de tuple string-int et qui retourne une liste de tuple string-int. La méthode retourne les trois tuples ayant le plus gros int en ordre descendant.

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/StructureLin%C3%A9aireCours1A24.zip?ref_type=heads