---
outline: deep
---

# Cours 4
# Objectifs
- Interface (pas les User Interface)
- Polymorphisme avec une interface
- Tp1
  
# Planification
<!-- no toc -->
- [Question d'amorce](#question-damorce)
- [Interface](#interface)
- [Git101](#git101)
- [Exercice](#travail-individuel)
- [Code fait en classe](#code-fait-en-classe)

# Question d'amorce

Vous devez ajouter une méthode de calcul de statistiques dans votre logiciel de gestion des olympiques.

3 personnes auront un calcul de statistiques.
1. Les athlètes
  - Basé sur leur classement.
2. Les entraineurs
  - Basé sur le nombre de médailles que les athlètes qu'ils entraînent remporte.
3. Les arbitres
  - Basé sur le nombre de compétition arbitrés.

- Impossible avec notre arbre d'héritage de faire une méthode commune aux trois. Soit je donne un calcul de stats aux bénévoles et employés, soit ce n’est pas la même méthode entre les joueurs/coach et l’arbitre.

# Interface

- Définition : Ensemble de fonctionnalités communes à plusieurs classes.
- Héritage = être et interface = avoir.
- Si un lien d'héritage peut être remplacé par un "EST", alors l'implémentation d'une interface peut être remplacé par un "A" ou "Possède la capacité de".
- Possibilité d'implémenter plusieurs interfaces, alors qu'une seule classe mère est possible.
- Les interfaces c'est comme un design par module

### En code

1. Implémenter des interfaces de Visual Studio (iComparable, iEnumerable, ...)
2. Créer des interfaces personnalisées
  - L'interface va contenir la signature des méthodes que les classes qui implémenteront l'interface devront implémenter. Comme pour les méthodes abstraites, l'interface impose un contrat avec la classe qui l'implémente.
  - Possible d'ajouter du code statique dans les interfaces afin d'éviter des répétitions.
3. Implémenter des interfaces personnalisées
4. Une fois implémenter les interfaces et la définition de leurs méthodes sont hérités comme n'importe quelle méthode.
5. Une fois que la structure est terminée, faire le diagramme de classe.

### Polymorphisme

Il est possible de transtyper un objet en interface pour permettre le polymorphisme.

# Tp1
### Git101

- Personnellement, j'utilise Git Extensions : https://gitextensions.github.io/

Sur Gitlab
1. Aller dans le repo de mon TP1
2. Choisir l'option "Fork" en haut à droite
3. Aller dans le repo du "Fork"
4. Dans le menu à gauche, sélectionner Manage -> Members
5. Choisir "Invite members" en haut à droite
6. Ajouter @JpBoucher comme "Reporter"
7. Revenir à la page principale du repo, sélectionner "Code" en haut à droite
8. Faire un clone with HTTPS du dépôt

À partir de Git Extensions
1. Dans la page d'accueil, sélectionner "Clone repository" dans le menu en bleu.
2. Normalement, le contenu que vous avez copier en #8 sera automatiquement collé dans "Repository to clone"
3. Sélectionner "Browse" sur la ligne de "Destination" et choisir le dossier dans lequel vous voulez avoir votre projet.
4. Sélectionner "Clone"
5. Commencer le travail!!

Raccourcis utile pour Git Extensions
- CTRL + Espace = Commit
- CTRL + Bas = Pull
- CTRL + Haut = Push
   
### Démo et code review du Tp1

- [Lien du Tp1](https://gitlab.com/progavance/contenu/-/blob/main/Tp1.pdf?ref_type=heads)

# Travail individuel

Ajoutez les méthodes suivantes dans votre code des exercices héritage - méthodes.

1. AjouterPrescription
  - Une méthode qui reçoit une string en paramètre et qui l’ajoute dans la liste des prescriptions d’un patient.  Au besoin ajouter l’attribut Prescriptions pour tous les patients.
2. Prescrire
  - Une méthode qui reçoit un patient et un médicament en paramètre et qui appelle la méthode AjouterPrescription du patient avec en paramètre le médicament à prescrire.  Seul les médecins et les pharmaciens peuvent prescrire.
3. Exporter
  - Méthode qui permet d’encrypter et d’exporter de façon sécuritaire de l’information pour un autre établissement de santé qui n’a pas accès au DSQ.  Voici les éléments qui peuvent être exportés :
    - Patients
    - Opérations
    - Radiographie
4. Archiver
  - Tous les éléments du DSQ ont une politique d’archivage.  Chacun des éléments à ses propres règles.
5. Afficher
  - Une méthode qui retourne une string qui contient le type d’individu suivi du nom et du prénom.  Par exemple : 
    - Patient – Boucher Jean-Philippe
    - Médecin – House Dr.
    - Patient longue durée – Crosby Sidney 
6. Dans votre main window
  - Créez les attributs suivants :
    - Une liste des intervenants (Peut contenir tous les intervenants de la santé ainsi que tous les types de patients)
    - Une liste d’actes médicaux (Peut contenir tous les types d’actes médicaux)
  - Créez les méthodes suivantes :
    - Afficher - Appel polymorphe de la fonction Afficher de chacun des éléments de la liste et ajout dans un listbox.
    - Archiver – Méthode qui permet d’archiver les deux listes.
    - ExporterPatient - Une méthode qui reçoit un patient en paramètre et qui l’exporte ainsi que tous les actes médicaux exportables qui le concerne.
    - ExporterTousLesPatients – Une méthode qui permet d’exporter tous les patients de la liste d’intervenants (Utilisez la méthode précédente)

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/HeritageA24%20Cours3.zip?ref_type=heads