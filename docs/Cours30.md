---
outline: deep
---

# Cours 30
  

# Correction Examen

```c#
public List<Personne> TrouverDesDegeneres(int pNombreDeDegeneres)
{
    Queue<Personne> fileConnaissances = new Queue<Personne>();
    List<Personne> degeneres = new List<Personne>();
    foreach (Personne connaissance in Connaissances)
        fileConnaissances.Enqueue(connaissance);
    while (fileConnaissances.Count > 0 && degeneres.Count < pNombreDeDegeneres)
    {
        if (fileConnaissances.Peek().aTropDArgent && fileConnaissances.Peek().estUnPeuCon)
            degeneres.Add(fileConnaissances.Peek());
        foreach (Personne connaissance in fileConnaissances.Dequeue().Connaissances)
            fileConnaissances.Enqueue(connaissance);
    }
    return degeneres;
}
```


```c#
private Cheval? ParierCommeUnMoron(List<Cheval> pChevaux, int ronde)
{
    if (ronde == 4 || pChevaux.Count<1)
        return null;
           
    int alea = Random.Shared.Next(100);
    foreach (Cheval cheval in pChevaux)
    {
        if (alea == cheval.ProbabiliteVictoire)
            return cheval;
    }
    foreach (Cheval cheval in pChevaux)
    {
        if (alea % cheval.Cote == 0)
            return cheval;
    }

    Cheval petiteCote = pChevaux[0];
    for (int i = 1; i < pChevaux.Count; i++)
    {
        if (petiteCote.Cote > pChevaux[i].Cote)
            petiteCote = pChevaux[i];
    }
    pChevaux.Remove(petiteCote);

    return ParierCommeUnMoron(pChevaux, ronde++);
}
```


```c#
public double TrouverMeilleurPrix()
{
    if (Intermediaires.Count == 0)
        return Prix;
    double meilleurPrix = Intermediaires[0].TrouverMeilleurPrix();
    double prixTempo = 0;
    for(int i = 1; i < Intermediaires.Count;i++)
    {
        prixTempo = Intermediaires[i].TrouverMeilleurPrix();
        if (prixTempo < meilleurPrix)
            meilleurPrix = prixTempo;
    }
    return meilleurPrix + Prix;
}
```


```c#
public void SeSeparerDunePersonneToxique(string pNomToxique, List<string> pNomEnfantsNonToxiques)
{
    Personne toxique = TrouverPersonneToxique(pNomToxique);
    for(int i =0; i<toxique.Connaissances.Count; i++)
    {
        if (pNomEnfantsNonToxiques.Contains(toxique.Connaissances[i].Nom))
        {
            toxique.Connaissances[i].Parent = toxique.Parent;
            toxique.Connaissances[i].Parent.Connaissances.Add(toxique.Connaissances[i]);
            toxique.Connaissances.Remove(toxique.Connaissances[i]);
        }
        else
            i++;
    }
    toxique.Parent.Connaissances.Remove(toxique);
    toxique.Parent = null;
}

private Personne TrouverPersonneToxique(string pNomToxique)
{
    foreach (Personne connaissance in Connaissances)
    {
        if (connaissance.Nom == pNomToxique)
            return connaissance;
    }
    Personne toxique;
    foreach (Personne connaissance in Connaissances)
    {
        toxique = connaissance.TrouverPersonneToxique(pNomToxique);
        if (toxique is not null)
            return toxique;
    }
    return null;

}
```
