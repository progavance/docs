---
outline: deep
---

# Cours 12

# Objectifs
- Comprendre le concept de structure de données
- Remplir votre coffre à outils
  
# Planification
<!-- no toc -->
- [Échauffement](#échauffement)
- [Structures de données](#structures-de-données-linéaire-avec-ordonnancement)
- [Exercices](#exercices)
- [Code du cours](#code-du-cours)

# Échauffement

Sur les tableaux blancs en équipe
1. Coder les méthodes Ajouter et DemarrerImpression
```c#
public class Imprimante
{
    /// <summary>
    /// Liste des documents à imprimer
    /// </summary>
    public List<string> documents { get; set; }

    /// <summary>
    /// Cette méthode permet d'ajouter un document dans la liste des documents
    /// à imprimer.
    /// </summary>
    /// <param name="pDocument">Le document à ajouter</param>
    public void Ajouter(string pDocument) 
    { 
    
    }

        /// <summary>
    /// Cette méthode est appelée pour lancer une impression. On imprime le document qui 
    /// est dans la liste depuis le plus longtemps
    /// </summary>
    public void DemarrerImpression()
    {
        
    }
```

2. Coder les méthodes RangerVetements et PrendreVetement
```c#
    public class TiroirDeJp
    {
        /// <summary>
        /// La liste des vetements dans le tiroir
        /// </summary>
        public List<Vetement> vetements{get; set;}

        /// <summary>
        /// Cette méthode permet d'ajouter des vetements dans le tiroir.
        /// </summary>
        /// <param name="pVetementsPropres">La liste à ajouter</param>
        public void RangerVetements(List<Vetement> pVetementsPropres)
        {

        }

        /// <summary>
        /// Cette méthode permet de prendre un vetement du tiroir.
        /// Au grand déplaisir de sa copine, Jp prend toujours le vetement du
        /// dessus (sans se poser de question sur l'agencement de ses habits)
        /// Celui sur le dessus est le dernier vêtement ajouté.
        /// </summary>
        /// <returns>Le vetement sur le dessus</returns>
        public Vetement PrendreVetement()
        {

        }
```

# Structures de données linéaire avec ordonnancement

- Il existe deux structures de données linéaire avec ordonnancement
- Les deux sont utilisés pour gérer des cas d'utilisation avec une gestion de l'ordre d'arrivée et de sortie
- Les deux ont une méthode précise pour insérer ou retirer un élément, ce n'est pas le dévelopeur qui choisit l'élément à retirer
- Les deux ne permettent pas d'accès par index

### La file 

- La file (ou queue)
  - Conserve l'ordre d'arrivée des éléments (FIFO)
  - Exemple
    - Queue d'impression
    - Playlist
    - Protocole de communication asynchrone
    - Parcours en largeur (broad)

### La pile 

- La pile (ou le stack)
  - L'ordre inverse d'arrivée des éléments (LIFO)
  - Exemple
    - Appel de méthodes
    - Page précédente dans un navigateur
    - Évaluation d'une formule mathématique
    - Parcours en profondeur (deep)

### En code

- Une souris est prisonnière d'un labyrinthe, elle doit retrouver son chemin.


```c#
private string leLab = "010000000000100\n010111101110110\n010000001000000\n011101111011111\n010000001010001\n010111111010101\n010100000010101\n010101101111101\n010101000000001\n010101110111101\n000100010101001\n010101010101111\n010101010100000\n010101010001110\n010100011111110";
List<List<int>> _grille = new List<List<int>>();
```

```c#
private void InitGrille()
{
    List<string> labSplit = leLab.Split('\n').ToList(); ;


    for (int i = 0; i < 15; i++)
    {
        _grille.Add(new List<int>());
        for (int j = 0; j < 15; j++)
        {
            if (labSplit[i][j] == '0')
                _grille[i].Add(0);
            else
                _grille[i].Add(1);
        }
    }
}
```

```c#
private void Dessiner()
{
    for (int i = 0; i < 15; i++)
    {
        for (int j = 0; j < 15; j++)
        {
            Rectangle laCase = new Rectangle();
            laCase.Width = 20;
            laCase.Height = 20;
            laCase.Stroke = new SolidColorBrush(Colors.Black);
            Canvas.SetLeft(laCase, i * 20);
            Canvas.SetTop(laCase, j * 20);
            switch (_grille[i][j])
            {
                case 0:

                    break;
                case 1:
                    laCase.Fill = new SolidColorBrush(Colors.Black);
                    break;
            }
             monCanvas.Children.Add(laCase);
        }
    }
}
```

# Exercices

1.	Écrivez une méthode RenverserPile qui reçoit une pile d’entiers et qui l’inverse.
2.	Écrivez une méthode FileEnPile qui reçoit une file d’entiers et retourne une pile avec le contenu dans le même ordre (le prochain élément à sortir est le même).
3.	Écrivez une méthode AjoutTricherFile qui permet d’ajouter un entier en tête d’une file d’entiers, l'entier et la file sont passés en paramètre.
4.	Écrivez une méthode AjoutTricherPile qui permet d’ajouter un entier au fond d’une pile d’entiers, l'entier et la pile sont passés en paramètre.
5.	Écrivez une méthode qui compare le contenu d’une pile d’entiers et d’une file d’entiers reçus en paramètre.  Si les entiers de la pile du sommet vers le bas sont identiques aux entiers de la file du premier au dernier, la fonction retourne vrai.  La pile et la file doivent demeurer intactes.
6.	Écrivez une méthode qui compare le contenu d’une pile d’entiers et d’une file d’entiers éléments par éléments et qui retourne les entiers identiques dans une liste liée triée en ordre croissant.  La pile et la file doivent demeurer intactes.


# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/PileFileA24.zip?ref_type=heads