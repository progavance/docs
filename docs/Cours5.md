---
outline: deep
---

# Cours 5

# Planification
<!-- no toc -->
- [Gestion d'erreurs](#gestion-derreurs)
- [Code fait en classe](#code-fait-en-classe)


# Gestion d'erreurs

### Journalisation

Enregistrement des actions effectuées dans un programme afin d'avoir une séquence de l'exécution. Permet de voir la séquence d'actions qui causent des erreurs, permet de valider la performance ou (si vous êtes un connard) permet la surveillance des utilisateurs.

Pourquoi faire une journalisation ? Parce que lorsque l'utilisateur dira que votre app est planté, il deviendra instantanément amnésique et il ne se souviendra pas de ce qu’il a fait, sur quoi il a cliqué ni dans quelle fenêtre il était.

Deux outils en VS : Trace et Debug. Ils ont un comportement semblable, sauf que Trace est exécuté en release.

Comment le faire en VS ?

1. Ajouter un fichier comme "listener" afin de sauvegarder la journalisation. 

```c#
private void InitFichierTrace()
{
    FileStream leFichier = File.Create("Fichier.txt");
    TextWriterTraceListener leListener = new TextWriterTraceListener(leFichier);
    Trace.Listeners.Add(leListener);
}
```
2. Ajouter l’instruction Trace.Writeline(…) aux endroits appropriés.

3. À la fermeture de votre app ajouter la commande Trace.Flush().

4. (Optionnel) Possibilité d'activer l'auto-flush. C'est plus couteux en terme de performance car vous faîtes plus de I/O, mais si le programme plante vous aurez les traces de l'exécution qui a causé le plantage. 

Références Microsoft : https://learn.microsoft.com/en-us/troubleshoot/developer/visualstudio/csharp/language-compilers/trace-and-debug

### Validation

Valider l'information saisie.

Pourquoi faire de la validation ? Parce que votre utilisateur ne sait pas attacher ses souliers à velcro.

Deux grands principes :
1. Valider le plus tôt possible.
2. Valider à toutes les étapes (back-end, front-end, ...).

Références Microsoft :
- Si vous utilisez Windows forms : https://learn.microsoft.com/en-us/dotnet/desktop/winforms/controls/errorprovider-component-windows-forms?view=netframeworkdesktop-4.8
- Si vous utilisez WPF : https://learn.microsoft.com/en-us/dotnet/desktop/wpf/data/how-to-implement-binding-validation?view=netframeworkdesktop-4.8

### Exceptions (try, catch, throw)

Permet de signaler une erreur et de continuer l'exécution du programme.

Pourquoi utiliser les exceptions ? Parce que les applications sont habituellement multi-services et aussitôt qu'on fait appel à des services externes on devrait se protéger.

Les principes :
- Lorsqu'on fait appel à un service externe (bd, I/O, ...) on imbrique le code dans un try catch.
- Le try catch n'est pas utilisé pour éviter des exceptions dans notre propre code
- Si on est dans une entité qui n'affiche pas (classe, module, ...) on throw.
- Si on est dans une entité d'affichage (forms, wpf, ...) on try catch et on affiche le message de l'exception.

Comment créer une exception personnalisée en VS ?

1. Créer une class serializable dans une autre classe ou dans son propre fichier de classe.
```c#
[Serializable]
class ExceptionPersonnalisée : Exception
```
2. Implémenter les 3 constructeurs de base d’une exception ainsi qu’un message par défaut.
```c#
[Serializable]
class ExceptionPersonnalisée : Exception
{
    private static readonly string MessageDefaut = "Le message par Defaut";
    public ExceptionPersonnalisée() : base(MessageDefaut) {}

    public ExceptionPersonnalisée(string leMessage) : base(leMessage) {}

    public ExceptionPersonnalisée(string leMessage, Exception innerException) : base(leMessage, innerException) {}
```
3. (Optionnel) Ajouter des attributs d’exceptions au besoin et créer des constructeurs personnalisés
```c#
private string Nom { get; set; }
private string UId { get; set; }

public ExceptionPersonnalisée(string pNom, string pUId) : base(MessageDefaut)
{
    Nom = pNom;
    UId = pUId;
}

```
4.	Lancer l'exception comme une exception régulière.


# Code fait en classe

https://gitlab.com/progavance/contenu/-/blob/main/GestionErreurA24.zip?ref_type=heads