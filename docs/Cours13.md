---
outline: deep
---

# Cours 13

# Objectifs
- Finir les nouvelles structures de données du bloc 2
- Remplir votre coffre à outils
  
# Planification
<!-- no toc -->
- [Retour examen 1](#retour-examen-1)
- [Structure de données associatives](#structures-de-données-associatives)
- [Exercices](#exercices)
- [Code du cours](#code-du-cours)

# Retour examen 1

- QAT final
  - Est-ce que c'est la note que j'attendais ? 
  - Est-ce que je dois changer ma préparation ?

- Les prochains examens
  - Comme Q3 et Q4

# Structure de données associatives

- La liste générique demeure une des meilleures structure en accès et cet accès est par index.
- Comment faire quand on ne connait pas l'index ? Par exemple, pour un accès par DA pour un des 6000 étudiants de Garneau ? 
- Il n'y a aucune structure de données linéaire qui puisse répondre efficacement à ce cas d'utilisation. On retombe dans un algo de recherche obligatoirement.
- Nous devrons alors utiliser une structure associative

### En code

- Faire une classe étudiant avec un DA et un mot de passe.
- Faire une méthode d'authentification
- Faire un while dégeux :(

### Le dictionnaire 

- Structure associative (ou à accès direct)
- Principe
  - Deux éléments dans la structure, une clé et une donnée (key-value)
  - Le champ clé doit être immuable
  - Exemple de clé :
    - ISBN
    - DA
    - Email pour une app Web
- Fonctionnement
  - La structure associative utilise une fonction de hachage pour convertir le champ clé dans l'index pour accéder à la donnée.
  - Une fonction de hachage est une fonction de calcul qui permet de renvoyer des résultats dans une limite spécifique (borne).
  - La fonction de hachage de la structure associative permet de distribuer le mieux possible les résultats pour éviter les collisions.
  - Une collision c'est quand deux clés donnent un index identique.
  - Ce sera impossible d'avoir une distribution parfaite, donc il y aura des collisions.

### Gestion des collisions

1. Chainage
   - Les données sont stockés dans une liste chainée. 
   - L'accès n'est techniquement plus direct, mais il est assez rapide pour qu'on parle d'accès direct.
2. Adressage ouvert
   - Recherche de la première case libre suite à une collision.
   - Va créer des agglomération de données.
   - Efficacité inégale.
3. Fonction de collisions
   - Couteux en temps

- Le dictionnaire de .NET : 
  - Utilise une liste chaînée pour gérer les collisions.
  - Appelle le gethashcode de la clé et parcourt la liste chainée en utilisant le equals pour retrouver la bonne donnée.

### En code

- Utilisation du dictionnaire de .NET
  - Spécifier les deux types (clé et donnée)
  - Utiliser le type du champ immuable pour la clé
  - Utiliser la type de ce qu'on veut stocker pour la donnée
  - TOUJOURS faire des validations avec le ContainsKey avant de faire un accès.

- Principales opérations avec le dictionnaire de .NET
  - .ContainsKey(clé)
  - accès direct avec la clé entre crochet
  - .Add(clé,valeur)
  - .Remove(clé)

- Si jamais vous voulez utiliser autre chose qu'un type primitif comme clé, vous devez :
  - Redéfinir la fonction GetHashCode
  - Trouver un GetHashCode qui distribue bien les résultats
  - Redéfinir le Equals afin que le Equals soit cohérent avec le GetHashCode
    - Si c'est Equals, le GetHashCode doit être pareil
    - Si le GetHashCode est pareil, c'est possible que le Equals ne le soit pas (ce sera alors une collision)

- Choses à éviter (seulement en dernier recours et si vous êtes forcé de le faire c'est peut-être que votre choix de structure n'est pas le bon)
  - Itérer avec un foreach sur un dictionnaire
  - Itérer avec un foreach sur les valeurs d'un dictionnaire
  - Extraire les clés ou les valeurs d'un dictionnaire

# Exercices

1.	Écrivez une méthode AjoutDansListe qui reçoit un dictionnaire de string-list d’object, une string et un object en paramètre.  Cette méthode ajoute l’object dans la liste de la clé passée en paramètre s’il n’y est pas déjà présent.  Cette méthode retourne un booléen qui indique si l’objet a été ajouté ou non.
2.	Écrivez une méthode AjoutDansPile qui reçoit un dictionnaire de integer-pile d’object, un integer et un object en paramètre.  Cette méthode insère l’object dans la bonne pile du dictionnaire.  Si la clé n’est pas dans le dictionnaire, la méthode doit faire l’instanciation d’une pile d’object et l’ajouter dans le dictionnaire avant de faire l’ajout de l’integer. Cette méthode retourne un booléen qui indique si la méthode a dû créer une nouvelle pile.
3.	Écrivez une méthode AdditionMultiple qui reçoit un dictionnaire de string-entier, une list de string et un entier.  Cette méthode parcourt la liste de string et utilise chaque string pour accéder au dictionnaire et incrémenter l’entier déjà présent de l’entier passé en paramètre. Cette méthode ne retourne rien. 
4.	Écrivez une méthode SommeListeEntier qui reçoit un dictionnaire de string-list d’entier en paramètre et une liste de string. Cette méthode parcourt la liste de string et utilise chaque string pour accéder à la liste d’entier du dictionnaire et en faire la somme. Elle retourne une file contenant la somme obtenue de chacune des string de la liste de string.

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/StructureAssociativeA24.zip?ref_type=heads