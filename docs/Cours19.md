---
outline: deep
---

# Cours 19

# Objectifs
- Être prêt pour mercredi prochain
  
# Planification
<!-- no toc -->
- [Info pour examen](#info-pour-examen)
- [Exercices bloc 2](#exercices-bloc-2)
- [Pratique examen](#pratique-examen)

# Info pour examen
- 4 questions
- 3h pous tous
- Tous de l'algo
- Les 4 structures seront présentes
  - Pile
  - File
  - Liste liée
  - Dictionnaire
- Notes du tp 2 et de l'examen 2 vendredi à 17h
  
# Exercices bloc 2

https://gitlab.com/progavance/contenu/-/blob/main/Exercices%20Bloc%202.zip?ref_type=heads

### Solution exercices bloc 2

https://gitlab.com/progavance/contenu/-/blob/main/Solutions%20Exercices%20Bloc%202.zip?ref_type=heads

# Pratique examen

https://gitlab.com/progavance/contenu/-/blob/main/PratiqueEx2.pdf?ref_type=heads

### Solution pratique examen 2

Q1
```c#
public List<Personne> AvecQuiAllerPrendreUneBiere(List<string> pAspects)
{
    List<Personne> retour = new List<Personne>();
    bool bTrouve = false;
    int i = 0;
    foreach (string aspect in pAspects)
    {   
        bTrouve = false;
        i = 0;
        do
        {
            if (Amis[i].Notes.ContainsKey(aspect) && Amis[i].Notes[aspect] > Notes[aspect] && !retour.Contains(Amis[i]))
            {
                retour.Add(Amis[i]);
                bTrouve = true;
                i = 0;
            }
            else
                i++;
        } while (!bTrouve && i< Amis.Count );
    }
    return retour;
}
```

Q2
```c#
 public Dictionary<string, List<double>> EvolutionDesPersonnages(Queue<Episode> pEpisodes)
 {
     Dictionary<string, List<double>> retour = new Dictionary<string, List<double>>();
     for (int i = 0; i < pEpisodes.Count; i++)
     {
         if (!retour.ContainsKey(pEpisodes.Peek().PersoPrincipale.Nom))
             retour.Add(pEpisodes.Peek().PersoPrincipale.Nom, new List<double>());
         retour[pEpisodes.Peek().PersoPrincipale.Nom].Add(pEpisodes.Peek().MoyenneAppreciation);
         pEpisodes.Enqueue(pEpisodes.Dequeue());
     }
     return retour;
 }
```

Q3
```c#
public List<Stack<DVD>> ReparerLaGaffeDuPetit(Stack<DVD> pileDVD)
{
    List<Stack<DVD>> retour = new List<Stack<DVD>>();
    Stack<DVD> pileRenverse = new Stack<DVD>();
    bool btrouve;
    int i;

    while (pileDVD.Count > 0)
        pileRenverse.Push(pileDVD.Pop());

    while (pileRenverse.Count > 0)
    {
        btrouve = false;
        i = 0;
        while (!btrouve && i < retour.Count)
        {
            if (retour[i].Peek().Saison == pileRenverse.Peek().Saison)
                btrouve = true;
            else
                i++;
        }
        if (!btrouve)
            retour.Add(new Stack<DVD>());
        retour[i].Push(pileRenverse.Pop());
    }
    return retour;
}
```

Q4
```c#
 public LinkedList<Episode> UltimeClassement(List<List<Episode>> pTousLesEpisodes)
 {
     LinkedList<Episode> classement = new LinkedList<Episode>();
     LinkedListNode<Episode> noeudCourant;
     foreach (List<Episode> saisons in pTousLesEpisodes)
     {
         foreach (Episode ep in saisons)
         {
             noeudCourant = classement.First;
             while(noeudCourant != null && noeudCourant.Value.MoyenneAppreciation > ep.MoyenneAppreciation)
                     noeudCourant = noeudCourant.Next;
             if (noeudCourant == null)
                 classement.AddLast(ep);
             else
                 classement.AddBefore(noeudCourant, ep);
         }
     }
     return classement;
 }
```