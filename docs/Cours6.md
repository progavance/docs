---
outline: deep
---

# Cours 6

# Objectifs
- Création de tests unitaires
  
# Planification
<!-- no toc -->
- [Tests unitaires](#tests-unitaires)
- [Code fait en classe](#code-fait-en-classe)

# Tests unitaires

- Les tests unitaires sont la première couche des tests d'une application. Ils testent une unité de code. Une unité de code peut être vu comme chaque chemin que le code peut prendre.
- Les tests unitaires peuvent être vu comme des mousquetons en escalade. Tu ne grimpes pas plus vite en plaçant des mousquetons, au contraire. Par contre, ça t'évites de reculer plus loin que le dernier mousqueton en cas de chute. Les tests unitaires sont pareils, ils ont un coût en temps, mais permettent d'éviter de trop reculer quand on tombe inévitablement sur un bug ou un changement de spécifications du client.

### Création du projet de tests

1.	À partir de la solution faire -> Nouveau projet –> Projet de tests NUnit
2.	Le placer dans la même solution
3.	S'assurer que c'est en .Net 8.0
4.	Propriétés du projet de test –> SE Cible -> Windows
5.	Ajouter la dépendance vers le projet réel

### Création des tests

1. Une classe de tests par classe du projet réel. Ajouter le préfixe Test pour nommer. Par exemple : TestJoueur pour une classe Joueur.
2. L'ajout se fait comme une nouvelle classe dans un projet régulier.
3. On fera une méthode de test par sortie possible d'une méthode publique.
4. Le nom des tests est super important, car il y aura beaucuop de tests dans une application. On nommera les tests de la façon la plus explicite possible. Par exemple : ConstructeurAvecMauvaiseDate ou ConstructeurAvecNomTropCourt.
5. Chaque méthode de test devra être précédée de [Test] pour que Visual Studio comprennent que c'est une méthode de test.
6. Les tests se feront avec des assertions, nous utiliserons principalement le Assert.That qui peut être vu comme un if.

### Exemple en code

```c#
public class TestPersonne
{
    [Test]
    public void ConstructeurJoueur()
    {
        Personne bob = new Personne("Bob");
        Assert.That(bob, Is.Not.Null);
        Assert.That(bob.nom == "Bob");
    }
}
```

### Tests des exceptions

1. Faire une méthode dans la classe de test qui cause l'exception. Ne pas l'identifier comme une méthode de test.
2. Faire une méthode Test qui fait un Assert.Throws en appelant la méthode créée en 1

### Exemple en code

```c#
public class TestPersonne
{
    [Test]
    public void ConstructeurNomTropPetit()
    {
        Assert.Throws<ArgumentException>(ArgumentException);
    }

    private void ArgumentException()
    {
        Personne bob = new Personne("");
    }
}
```


# Code fait en classe

https://gitlab.com/progavance/contenu/-/blob/main/TestUnitairesA24.zip?ref_type=heads