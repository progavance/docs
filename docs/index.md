---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "ProgAvance"
  text: "Notes pour ProgAvance"
  tagline: 
  actions:
    - theme: brand
      text: Cours 1
      link: /Cours1
    - theme: brand
      text: Cours 2
      link: /Cours2
    - theme: brand
      text: Cours 3
      link: /Cours3
    - theme: brand
      text: Cours 4
      link: /Cours4    
    - theme: brand
      text: Cours 5
      link: /Cours5 
    - theme: brand
      text: Cours 6
      link: /Cours6 
    - theme: brand
      text: Cours 7
      link: /Cours7 
    - theme: brand
      text: Cours 8
      link: /Cours8 
    - theme: brand
      text: Cours 9
      link: /Cours9 
    - theme: brand
      text: Cours 10
      link: /Cours10 
    - theme: brand
      text: Cours 11
      link: /Cours11 
    - theme: brand
      text: Cours 12
      link: /Cours12 
    - theme: brand
      text: Cours 13
      link: /Cours13
    - theme: brand
      text: Cours 14
      link: /Cours14 
    - theme: brand
      text: Cours 15
      link: /Cours15 
    - theme: brand
      text: Cours 16
      link: /Cours16
    - theme: brand
      text: Cours 17
      link: /Cours17  
    - theme: brand
      text: Cours 18
      link: /Cours18
    - theme: brand
      text: Cours 19
      link: /Cours19
    - theme: brand
      text: Cours 20
      link: /Cours20
    - theme: brand
      text: Cours 21
      link: /Cours21
    - theme: brand
      text: Cours 22
      link: /Cours22
    - theme: brand
      text: Cours 23
      link: /Cours23
    - theme: brand
      text: Cours 24
      link: /Cours24
    - theme: brand
      text: Cours 25
      link: /Cours25
    - theme: brand
      text: Cours 26
      link: /Cours26
    - theme: brand
      text: Cours 27
      link: /Cours27
    - theme: brand
      text: Cours 28
      link: /Cours28
    - theme: brand
      text: Cours 29
      link: /Cours29
    - theme: brand
      text: Cours 30
      link: /Cours30
features:
  - title: Tp 3
    link : https://gitlab.com/progavance/contenu/-/blob/main/Tp3A24.pdf?ref_type=heads
    details: Tp 3

  - title: Orchestration
    link : https://gitlab.com/progavance/contenu/-/blob/main/orchestration.png?ref_type=heads
    details: Contenu cours par cours

  - title: Plan de cours
    link : https://gitlab.com/progavance/contenu/-/blob/main/Plan%20de%20cours%20A23.pdf?ref_type=heads
    details: Le plan de cours
---

