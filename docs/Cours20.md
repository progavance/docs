---
outline: deep
---

# Cours 20
  
# Planification
<!-- no toc -->
- [QAT - Début](#questionnaire-dattribution-causale-début)
- [Examen]
- [QAT - Fin](#questionnaire-dattribution-causale-fin)

# Questionnaire d'attribution causale début

1. Quelle était ma note de l'examen 1 ?
2. Est-ce que j'étais satisfait de ma note ?
3. Qu'est-ce que j'ai changé dans ma préparation ?

# Examen

# Questionnaire d'attribution causale fin

1. Qu'est-ce qui a bien été ?
2. Qu'est-ce qui a moins bien été ?
3. Je m'attends à une note de :
