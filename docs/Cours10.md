---
outline: deep
---

# Cours 10
  
# Planification
<!-- no toc -->
- [QAT - Début](#questionnaire-dattribution-causale-début)
- [Examen]
- [QAT - Fin](#questionnaire-dattribution-causale-fin)

# Questionnaire d'attribution causale début

1. Est-ce que je suis prêt ?
2. Pourquoi je suis prêt ou non ?
3. Comment je me suis préparé ?

# Examen

# Questionnaire d'attribution causale fin

1. Qu'est-ce qui a bien été ?
2. Qu'est-ce qui a moins bien été ?
3. Je m'attends à une note de :
