---
outline: deep
---

# Cours 26

# Objectifs
- Création d'une structure arborescente personnalisée
  
# Planification
<!-- no toc -->
- [Arbre binaire](#arbre-binaire)
- [Code du cours](#code-du-cours)

# Arbre binaire

- Un arbre avec un maximum de 2 enfants par noeud
- Toujours tendre vers la recherche dichotomique

### En code

- Classe générique qui impose un type avec IComparable
- Ajouter un élément
- Trouver un élément
- Min
- Max
- Poids
- Hauteur
- Supprimer un élément

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/ArbreBinaireA24.zip?ref_type=heads