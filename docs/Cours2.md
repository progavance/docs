---
outline: deep
---
# Cours 2
# Objectifs
- Revisiter le modèle objet et l'architecture d'une app objet
- POO : le prochain niveau
  
# Planification
<!-- no toc -->
- [Récapitulatif](#petit-récapitulatif-du-modèle-objet)
- [Amorce de l'héritage](#activité-damorce)
- [Héritage](#héritage)
- [Exercice](#travail-individuel)
- [Code fait en classe](#code-fait-en-classe)

# Petit récapitulatif du modèle objet

- Les étapes de création d'un objet
    1. On crée un modèle
        - En code : on crée une classe
        - Dans la monde physique : on crée une usine
    2. Création d'un espace pour contenir l'objet
        - En code : on alloue un espace en mémoire (MaClasse monObjet)
        - Dans la monde physique : on réserve un espace dans un entrepôt
    3. On construit l'objet
        - En code : on crée l'instance de la classe en appellant le constructeur (monObjet = new MaClasse())
        - Dans la monde physique : on crée l'objet et on le place dans l'entrepôt
        - Le terme instance est important, car l'objet crée en programmation demeure lié et dépendant du modèle.
        - Dans le monde physique l'objet une fois crée devient indépendant du moule/usine qui l'a créé.

- Une classe est un moule ou une usine qui nous permet de produire des objets semblables mais différents.
  - Les caractéristiques (attributs) seront différentes d’un objet à l’autre.
  - Les comportements (méthodes) seront identiques, mais ils peuvent être personnalisés via les attributs d’une instance.
- Composition ou Encapsulation d’objets
  - Utilisation d’une classe au sein d’une autre
  - "Est composé de"

# Activité d'amorce

Vous obtenez le contrat pour faire le logiciel de gestion pour les prochains olympiques qui se tiendront en Italie.
- Le logiciel doit gérer les athlètes
  - CRUD d’un athlète
  - Afficher les résultats d'un athlète
  - Classement des athlètes (attention certaines disciplines classent selon le poids)
 
- Le logiciel doit gérer les entraîneurs
  - CRUD d’un entraîneur
  - Lier un entraîneur avec un athlète
  - Valider les accréditations d'un entraîneur
 
- Le logiciel doit gérer les employés
  - CRUD d'un employé
  - Affectation d'un employé à une tâche
  - Production de l’horaire d'un employé

- Le logiciel doit gérer les bénévoles
  - CRUD d'un bénévole
  - Affectation d'un bénévole à une tâche
  - Production de l’horaire d'un bénévole
   
- Le logiciel doit gérer les arbitres
  - CRUD d’un arbitre
  - Affectation d'un arbitre à une tâche
  - Production de l’horaire d'un arbitre
  - Validation que les arbitres sont neutres (ne viennent pas du même pays qu’un des athlètes arbitrés)

# Héritage

Objectif : simplifier la modélisation lorsque plusieurs classes se ressemblent beaucoup. 

- Héritage
  - Définition d’une hiérarchie entre les classes semblables
  - « Est »
- Façon de faire l’héritage
  - Définition d’une classe mère avec les attributs et méthodes communes
  - Définition de classes dérivées avec les attributs et méthodes spécifiques
  - Une seule maman possible
  - Plusieurs enfants possibles
- Avantage de l’héritage
  - Éviter la redondance de code
  - Simplifie la modélisation
  - Offre de nouvelles possibilités pour la définition et le traitement des objets (polymorphisme)
- Ajout d’un niveau d’accès propre à l’héritage
  - Private : Restreint à la classe.
  - Protected : Restreint à la classe et à ses enfants.
  - Internal : Public dans le .dll.
  - Public : Public partout.
- Classes abstraites (abstract)
  - Permet de définir des classes non-instanciables qui regroupe des attributs/méthodes de plusieurs autres classes.
  - Permet d’éviter la redondance.

# Constructeurs dans une structure d'héritage

- Chaque classe est responsable d'initialiser les attributs qu'elle définit.
- Les classes enfants doivent faire appel au constructeur de leur classe parent pour initialiser les attributs définis dans la classe parent. Ils peuvent toutefois écraser la valeur au besoin.
- Ainsi, dans une structure de classe en héritage, les objets se construisent par une cascade d'appel aux constructeurs qui commence par la classe sans parent et qui descend jusqu'à la classe que l'on veut instancier. 

# Travail individuel

1.	Annoncé depuis milles ans et pas encore terminé, le projet de dossier santé Québec aurait bien besoin d’un coup de main.  Les principaux intervenants dans ce système sont :
- Patient courte durée
  - NoAssMaladie
  - Nom
  - Prénom
  - …
- Patient longue durée
  - Pareil que le patient courte durée, mais avec les informations d’occupation de chambre en plus
- Médecin
  - Id
  - IdEtablissement
  - Nom
  - Prénom
  - …
- Infirmier
  - Id
  - IdEtablissement
  - Nom
  - Prénom
  - …
- Pharmacien
  - Id
  - IdEtablissement
  - Nom
  - Prénom
  - …
- Établissement
  - Id
  - Tous les champs de localisation d’un établissement
  - Liste d’employés

Définissez les classes du système afin de pouvoir instancier chacun des intervenants.  Codez les attributs, les accesseurs et les constructeurs.  Tous les constructeurs de classe sont des constructeurs paramétrés avec tous les attributs de la classe en paramètre.

# Code fait en classe

https://gitlab.com/progavance/contenu/-/blob/main/HeritageA24.zip?ref_type=heads