---
outline: deep
---

# Cours 16

# Objectifs
- Délégués
  
# Planification
<!-- no toc -->
- [Délégué](#délégué)

# Délégué 
- Parlons encapsulation et architecture de code
  - Qu’en est-il de la séparation de la logique de code? Habituellement, la logique applicative (backend) sera séparée de la logique d’affichage (frontend). 
  - On veut ramener la logique applicative le plus proche possible de son modèle. Le code qui concerne une personne doit être dans la classe personne autant que possible.
  - Les structures de données amènent ainsi un problème vu que la classe = le modèle d'une instance et que la structure = un ensemble d’instances. Donc, toute la logique applicative qui concernent plus d'une instance ne peut être mise dans la classe qui représentent une seule instance. Par exemple : la recherche dans une structure de données ou le filtrage d'une structure de données.
  - Solution : les délégués (delegate)

### Solution de départ

https://gitlab.com/progavance/contenu/-/blob/main/D%C3%A9l%C3%A9gu%C3%A9D%C3%A9part.zip?ref_type=heads

### 3 types de délégué
- Prédicat 
  - 1 objet typé en paramètre 
  - Un bool en retour
- Action
  - 1 objet typé en paramètre 
  - Pas de retour
- Comparaison 
  - 2 objets typés en paramètre 
  - Un int en retour

### En code

- Predicat
  - FindAll -> Inclinaison nulle, LargeurDispo, EnRabais
  - RemoveAll
  - Les finds
- Action
  - ForEach -> ChangerPrix
- Comparaison
  - Sort -> Trier ascendant/descendant par marque/prix
  - Parler du IComparable, sort() utilise le IComparable.

### Avantages vs coder dans un foreach dans le UI

- Ramène le code dans la couche applicative (classes) au lieu de la couche d'affichage (windows, forms, ...)
- Réutilisable à plusieurs endroits dans la couche d'affichage
- Réutilisable dans plusieurs couche d'affichage différentes (app Web, app mobile, intranet, ...)
- Réutilisable pour différentes structures de données

### Désavantages vs coder dans un foreach dans le UI

- Moins permissif vu que les délégués ne sont pas paramétrables. Par exemple, prix dans un intervalle défini par l'utilisateur
- Requiert une vision plus globale du contexte
- Plus difficile à lire pour ceux qui ne sont pas habitués avec des architectures multicouches

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/D%C3%A9l%C3%A9gu%C3%A9Fin.zip?ref_type=heads