---
outline: deep
---

# Cours 9

# Objectifs
- Être prêt pour mercredi prochain
  
# Planification
<!-- no toc -->
- [Retour Tp1](#retour-tp1)
- [Échauffement pré-examen](#échauffement-pré-examen)
- [Dossier Santé Québec](#dossier-santé-québec)
- [Pratique examen](#pratique-examen)

# Retour Tp1
1. Héritage 
2. For et While
3. Qualité de code
   
# Échauffement pré-examen

En équipe, sur les tableaux blancs.
1. Le nombre 55 possède les deux propriétés suivantes :  si on lui soustrait 1 on obtient un multiple de 9 et si on lui ajoute 1 on obtient un multiple de 8. Codez une méthode qui trouve le plus petit nombre de 4 chiffres possédant la même propriété ?

2.	Écrivez une méthode qui retourne True si la chaîne passée en paramètre est un palindrome. Vous devez utiliser la string comme un vecteur de char et non pas comme un objet de VS. Donc, vous pouvez seulement utiliser le .length et les []. Pas de reverse ou autre truc du genre.

3.	Écrivez une méthode qui retourne True si le vecteur d’entier positif passée en paramètre comporte un point d’équilibre.

# Dossier Santé Québec

https://progavance.gitlab.io/docs/Cours2.html#travail-individuel

https://progavance.gitlab.io/docs/Cours3.html#travail-individuel

https://progavance.gitlab.io/docs/Cours4.html#travail-individuel



# Pratique examen

https://gitlab.com/progavance/contenu/-/blob/main/PratiqueEx1.pdf?ref_type=heads
