---
outline: deep
---

# Cours 23

# Objectifs
- Récursivité ++

# Le cours en une image

![Les tris](/images/tri.jpg)  

# En équipe sur les tableaux blancs

- Coder le tri rapide
  - À partir d'un élément pivot, la liste est divisée en deux listes, une pour les éléments plus petits que le pivot et une autre pour les éléments plus grands.
  - On réapplique cette logique sur chacune des deux listes.
  - Une liste de 0 ou 1 élément est triée.
- Ensemble en code


# En équipe sur les tableaux blancs
- Coder le tri fusion
  - On divise la liste jusqu'à créer une série de liste contenant un seul élément.
  - On fusionne chacune des listes en les triant au fur et à mesure
  - Exemple en image :
![Le tri fusion](/images/trifusion.png)  
- Ensemble en code

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/TriA24.zip?ref_type=heads
