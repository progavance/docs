---
outline: deep
---

# Cours 3

# Objectifs
- Les méthodes avec l'héritage
- Polymorphisme
  
# Planification
<!-- no toc -->
- [Récapitulatif](#petit-récapitulatif-du-modèle-objet)
- [Amorce de l'héritage](#activité-damorce)
- [Héritage](#héritage)
- [Exercice](#travail-individuel)
- [Code fait en classe](#code-fait-en-classe)

# Petit récapitulatif du modèle objet

Les méthodes régulières
1. Comportement commun à toutes les instances de la classe.
2. Malléable via les paramètres.
3. Max 30-35 lignes 
4. Nommer avec un verbe d’action précis.
4. Public (accès à ceux qui utilisent l’instance)
5. Private (méthodes utilitaires pour des méthodes publiques)

Deux concepts plus avancés :
1. Overload : Une méthode avec plusieurs signatures. Elle fait toujours le même comportement, mais elle permet des paramètres différents selon les différentes signatures.
2. Override (ToString, Equals, GetHashCode) : Vous utilisiez de l'héritage depuis le début, toutes les classes héritent par défaut de la classe Object. Override c'est le principe de changer la définition d'une méthode héritée. 


# Activité d'amorce

Vous devez ajouter les 3 méthodes suivantes dans votre logiciel de gestion des olympiques.
1. EnvoyerLettreRemerciement
  - La lettre est envoyée à tous les ressources humaines
  - C'est une lettre de base identique pour tous.
  - Les bénévoles ont une version différente de la lettre, il y a un paragraphe de plus à la fin.
2. Afficher
  - Tous les utilisateurs du système doivent pouvoir s'afficher.
  - L'affichage est : Nom, Prénom - Type d'utilisateur.
3. ValiderÉligibilité
  - Les joueurs et les entraîneurs doivent être validés avant de pouvoir prendre part à une compétition.
  - Les validations sont différentes (âge/poids/sexe pour le joueur, accréditation pour l'entraîneur)

# Héritage

Trois types de méthodes pour l'héritage :
1. Abstraite (abstract) : Méthode non-implémentée dans la classe qui la définie. Méthode qui doit être obligatoirement être overridée dans chacune des classes enfants qui héritent de la classe qui définit la méthode.
2. Virtuelle (virtual) : Méthode implémentée dans la classe qui la définie. Méthode qui peut être redéfinie dans les classes enfants qui héritent de la classe qui définit la méthode.
3. Redéfinie (override) : Méthode  qui redéfinit une méthode implémentée ou non dans une classe parent.

- La question importante devient où placer les méthodes ? La réponse est la même que celle de la question : Qui peut faire cette méthode ?

### Polymorphisme

Tout ça juste pour éviter des répétitions ? Noui

Le polymorphisme c'est le principe de faire un traitement sur un ou plusieurs objets sans avoir à valider le type. Une bonne structure de classes en héritage permet de simplifier grandement les algorithmes qui vont utiliser les instances de ces classes.

Exemples avec les 3 méthodes de l'amorce.

Dans certains cas on voudra tout de même valider le type des instances. Vous pouvez le faire avec l'opérateur "IS". Le IS retourne un booléen qui valide si un objet EST une instance de la classe validée.

Une fois que vous validez avec le IS, vous avez le droit de changer le type de l'instance avec un cast. Par contre, vous devez toujours favoriser le polymorphisme au IS + cast.

# Travail individuel

1. Ajoutez les éléments suivants dans votre code des exercices héritage - attributs 
  - ExamenRoutine
    - IdPatient
    - IdMedecin	
    - Date
    - ConstatMedecin
    - …
  - Opération
    - IdPatient
    - IdMedecin
    - Date
    - NomOperation
    - Duree
    - …
  - RencontreMedPat (Toute rencontre entre médecin et un patient qui n’est pas un examen de routine)
    - IdPatient
    - IdMedecin	
    - Date
    - ConstatMedecin
    - …
  - Radiographie
    - IdPatient
    - IdMedecin
    - IdTechnicien
    - Date
    - …

Ajoutez les méthodes suivantes dans votre code des exercices héritage - attributs.

1. CalculerPaye
  - Une méthode qui retourne la paye (il y en a 26 dans une année) d’un employé.  Contrairement aux pharmaciens et aux infirmières, les médecins peuvent avoir une prime d’éloignement qui s’ajoute à leur salaire de base.  Au besoin ajouter l’attribut salaire à tous les employés et l’attribut prime au médecin.
2. CalculerCoutChambre
  - Une méthode qui retourne le coût d’occupation d’une chambre pour un patient de longue durée.  Au besoin ajouter les attributs DateDébut, DateFin et CoutChambre au patient longue durée.
3. CalculerCoutTraitement
  - Une méthode qui permet de calculer et retourner le coût de traitement d’un patient.  Ajouter les attributs Operation (integer), Consultation (integer) et Radiographie (integer) aux patients.  La règle de calcul est la suivante : 10 x Consultation + 100 x Radiographie + 1000 x Operation + le coût de la chambre
4. TransfertLongueDuree
  - Une méthode qui permet à un médecin d’admettre un patient normal en patient longue durée.  La fonction reçoit un patient normal et retourne un patient longue durée.  L’attribut DateFin du patient longue durée n’est évidemment pas dans le constructeur de PatientLongueDuree
5. PriseRV
  - Méthode qui permet de trouver un rendez-vous dans l’horaire pour les opérations, radiographie et examen de routine.

# Code fait en classe

https://gitlab.com/progavance/contenu/-/blob/main/HeritageA24%20Cours2.zip?ref_type=heads