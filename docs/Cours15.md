---
outline: deep
---

# Cours 15

# Objectifs
- Génériques
  
# Planification
<!-- no toc -->
- [Deux prochaines semaines](#deux-prochaines-semaines)
- [Tp2](#tp2)
- [Algorithmie modulaire](#algorithmie-modulaire)
- [Génériques](#Génériques)
- [Code du cours](#code-du-cours)

# Deux prochaines semaines
- Lundi 21 octobre
  - Génériques
  - Tp 2h
- Mercredi 23 octobre
  - Délégués
  - Tp 2h
- Lundi 28 octobre
  - Consolidation : liste liée ++
  - Tp 2h
- Mercredi 30 octobre
  - Consolidation : liste liée ++
  - Tp 2h
  - Remise Tp2 17h

# Tp2
- Inconfort inhérent de l'apprentissage
- Zone proximale de développement et travail d'équipe

# Algorithmie modulaire
- Découper et réutiliser autant que possible.
- Penser les algorithmes commes un ensemble de modules qu'on peut réutiliser et recombiner 
- Éviter les algorithmes de style boîtes noires.
  - Déboggage long
  - Tests unitaires inefficaces
  - Réutilisation quasi-impossible
  - Algorithmes à usage unique
1. Être lâche (ne pas réécrire)
2. Les petites méthodes c'est sexy :heart_eyes:

### En code - D'où on part
- Solution de départ : https://gitlab.com/progavance/contenu/-/blob/main/DepartGeneriqueA24.zip?ref_type=heads
- Mélanger les athlètes
- Mélanger les arbitres
- CalculerNotePlongeon

# Génériques
- Langages typés vs non-typés
- Rendre un comportement indépendant du type des éléments afin de faire des traitements dits "agnostiques", soit qui pourront être réutilisés peu importe le type.
- La liste générique (list<>) est un bon exemple d'utilisation des génériques. Elle fonctionne peu importe le type des éléments.
- Nécessaire pour que le découpage d'algorithme soit encore plus efficace.
- Classe et méthode peuvent être génériques.


### En code - Où on veut aller
- Mélanger générique
- TrouverValeurMin générique (avec IComparable ou IStats)
- TrouverValeurMax générique (avec IComparable ou IStats)
- Moyenne générique (avec IStats)
- Permuter générique

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/FinGeneriqueA24.zip?ref_type=heads