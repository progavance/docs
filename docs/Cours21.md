---
outline: deep
---

# Cours 21

# Planif bloc 3
- Lundi 11 nov - Récursivité
- Mercredi 13 nov - Arbres
- Lundi 18 nov - Récursivité++
- Mercredi 20 nov - Présentation Tp3


# Objectifs
- Mes premiers pas avec la récursivité
  
# Planification
<!-- no toc -->
- [Retour examen 2](#examen-2)
- [Échauffement](#échauffement)
- [Récursivité](#la-récursivité)
- [Exercices](#exercices)
- [Code du cours](#code-du-cours)

# Examen 2

Solutionnaire :

Q1
```c#
public void PurifierAccueil(string pTagVoulu)
{
    LinkedListNode<Contenu> noeudcourant;
    LinkedListNode<Contenu> suivant;
    for (int i = 0; i < PageAccueil.Count; i++)
    {
        noeudcourant = PageAccueil.Peek().First;
        while (noeudcourant != null)
        {
            if (noeudcourant.Value.Tags.Contains(pTagVoulu))
            {
                suivant = noeudcourant.Next;
                PageAccueil.Peek().Remove(noeudcourant);
                noeudcourant = suivant;
            }
            else
                noeudcourant = noeudcourant.Next;
        }
        PageAccueil.Enqueue(PageAccueil.Dequeue());
    }
}
```

Q2
```c#
private void CorrectionPsychee(List<Memoire> pMemoiresCreatives)
{
    Stack<Memoire> tempo = new Stack<Memoire>();
    int memeAjout = 0;
    foreach (Stack<Memoire> meme in _psychee)
    {
        for (int i = 0; i < meme.Count; i++)
        {
            if (meme.Peek().influence < 0)
                meme.Pop();
            else
                tempo.Push(meme.Pop());
        }
        meme.Push(pMemoiresCreatives[memeAjout]);
        memeAjout++;
        for (int i = 0; i < tempo.Count / 2; i++)
            meme.Push(tempo.Pop());
        meme.Push(pMemoiresCreatives[memeAjout]);
        memeAjout++;
        while (tempo.Count > 0)
            meme.Push(tempo.Pop());
    }
}
```

Q3
```c#
private void AjusterValeur(string pNomValeur, int ajustementImportance)
{
    LinkedListNode<Valeur> noeudCourant = valeurs.First;
    LinkedListNode<Valeur> precedent;
    bool bTrouve = false;
    while (noeudCourant is not null && !bTrouve)
    {
        if (noeudCourant.Value.nom == pNomValeur)
        {
            bTrouve = true;
            noeudCourant.Value.importance += ajustementImportance;
            precedent = noeudCourant.Previous;
            while (precedent is not null && precedent.Value.importance < noeudCourant.Value.importance)
            {
                valeurs.Remove(noeudCourant);
                valeurs.AddBefore(precedent, noeudCourant.Value);
                precedent = noeudCourant.Previous;
            }
        }
        noeudCourant = noeudCourant.Next;
    }
}
```

Q4
```c#
private void AutoApprentissage(string pMot)
    {
        Dictionary<string, int> statsMots = new Dictionary<string, int>();
        if (_connaissances.ContainsKey(pMot))
        {
            foreach (string mot in _connaissances[pMot])
            {
                foreach (string mot2 in _connaissances[mot])
                {
                    if (!_connaissances[pMot].Contains(mot2))
                    {
                        if (statsMots.ContainsKey(mot2))
                            statsMots[mot2]++;
                        else
                            statsMots.Add(mot2, 1);
                    }
                }
            }
            KeyValuePair<string, int> meilleurMot = new KeyValuePair<string, int>("", -1);
            foreach (KeyValuePair<string, int> mot in statsMots)
            {
                if (mot.Value > meilleurMot.Value)
                    meilleurMot = mot;
            }
            _connaissances[pMot].Add(meilleurMot.Key);
        }
    }

```

# Échauffement

Sur les tableaux blancs en équipe, coder les méthodes Factorielle et Fibonnaci

```c#
/// <summary>
/// Cette méthode permet de trouver la factorielle du
/// nombre passé en paramètre. La factorielle d'un nombre
/// est le produit des nombres entiers positifs inférieurs 
/// ou égaux à lui même. Ainsi la factorielle de 5 est :
/// 5*4*3*2*1
/// </summary>
private int Factorielle(int pNombre)
{

}
```

```c#
/// <summary>
/// La suite de fibonacci est une suite de nombre dans 
/// laquelle chaque nombre est la somme des deux précédents.
/// Elle commence par 0 et 1, puis elle se poursuit avec
/// la somme des deux nombres précédents.
/// 
/// Cette méthode permet de trouver le Nème terme de la suite
/// de Fibonnaci. Par exemple, si 7 est passée en paramètre,
/// la méthode retournera 8 car :
/// 0 - 1 - 1 - 2 - 3 - 5 - 8
/// </summary>
private int Fibonacci(int pNombre)
{

}
```

# La récursivité

- Quelle était la devise de Machiavel ?
- Comment je peux manger un gateau de 10 mètres ?
- Comment je peux monter une montagne de 20000 mètres ?
- Il y a deux gros tas d'argent, vous devez en choisir un seul. Si vous optez pour le plus gros vous le gardez, sinon vous n'avez rien. Vous êtes capable de compter jusqu'à 10, pas plus. Que faîtes-vous ?
- Définition : une méthode qui s'appelle elle-même pour trouver la solution à un problème

### Principes pour la récursivité

- Découper un problème complexe en petits problèmes plus digestes
- Chaque appel de méthodes réduit le problème jusqu'à obtenir une condition d'arrêt
- La condition d'arrêt est atteint quand le problème devient tellement simple que la réponse est directe.
- Ainsi, les 3 éléments essentiels d'une méthode récursive sont :
  - Une condition d'arrêt
  - Une réduction du problème à chaque appel afin de rapprocher de la condition d'arrêt
  - Un appel récursif

### Forces et faiblesses de la récursivité

- Les méthodes récursives sont sexy :heart_eyes:
- Ce n'est pas applicable dans tous les cas
- C'est parfois une solution couteuse et il faut réfléchir aux conséquences, notamment sur l'optimisation, quand on utilise la récursivité

### En code

- Factorielle récursive
- Fibonacci récursif
  - Tellement sexy! :heart_eyes:
  - Mais tellement poche :disappointed:
- Fibonnaci sur les stéroïdes


# Exercices

1. Codez une méthode récursive SommeDesUnites qui reçoit une string en paramètre et qui retourne la somme de tous les chiffres de la string. Par exemple, la string 13487 retournerait 23 vu 1 + 3 + 4 + 8 + 7 = 23. Ne faîtes pas de validation, partez du principe que la string ne contient que des chiffres.
2. Codez une méthode récursive Palindrome qui valide si un mot passé en paramètre est un palindrome. 
3. Codez la méthode récursive PlusGrandDiviseurCommun (pgdc) qui reçoit en paramètre deux valeurs entières et qui retourne un integer contenant le pgdc.
Utilisez la formule suivante : Soit X et Y les deux valeurs entières.  Si X > Y le pgdc de ces deux nombres est le même que celui de X MOD Y et de Y.
4. Soit une classe Personne telle que :

```c#
public class Personne
{
    public string Nom {  get; set; }
    public List<Personne> Enfants { get; set; }
```
Codez une méthode récursive à l’extérieur de la classe Personne qui permet de trouver un enfant selon son nom. Cette méthode reçoit une personne en paramètre et parcourt tous ses enfants, direct ou non, et retourne la bonne personne selon le nom.

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/RecursiviteA24.zip?ref_type=heads