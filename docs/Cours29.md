---
outline: deep
---

# Cours 29

# Objectifs
- Être prêt pour mercredi
  
# Planification
<!-- no toc -->
- [Correction exercices](#correction-exercices)
- [Pratique examen](#pratique-examen)

# Correction exercices

- https://progavance.gitlab.io/docs/Cours21.html#exercices
- https://progavance.gitlab.io/docs/Cours22.html#exercices

### Cours 21 

```c#
public double SommeDesUnites(string pString)
{
    double retour = Double.Parse(pString[0].ToString());
    if (pString.Length == 1)
        return retour;
    else
        return retour + SommeDesUnites(pString.Substring(1));
}
```

```c#
public bool Palin(string pString)
{

    if (pString.Length < 2)
        return true;
    if (pString[0] != pString[pString.Length-1])
        return false;
    return Palin(pString.Substring(1,pString.Length-2));
}
```

```c#
public int PGDC(int pX, int pY)
{
    if (pX < pY)
    {
        int tempo = pX;
        pX = pY;
        pY = tempo;
    }
    if (pX % pY == 0)
        return pY;
    return PGDC(pY, pX % pY);
}
```

```c#
public Personne TrouverParNom(Personne pPers, string pNom)
{
    foreach (Personne enfant in pPers.Enfants)
    {
        if (enfant.Nom == pNom)
            return enfant;
        Personne retour;
        retour = TrouverParNom(enfant, pNom);
        if (retour != null)
            return retour;
    }
    return null;
}
```
### Cours 22

```c#
public void ParcoursHauteurNonRecu(Noeud pRacine)
{
    Stack<Noeud> pile = new Stack<Noeud>();
    pile.Push(pRacine);

    while (pile.Count > 0)
    {
        Noeud tempo = pile.Pop();
        Console.WriteLine(tempo.Valeur);
        for (int i = tempo.Enfants.Count-1; i >= 0; i--)
            pile.Push(tempo.Enfants[i]);
    }
}
```

```c#
public void ParcoursHauteurRecu(Noeud pNoeud)
{
    Console.WriteLine(pNoeud.Valeur);
    foreach (Noeud enfant in pNoeud.Enfants)
        ParcoursHauteurRecu(enfant);
}
```

```c#
public void ParcoursLargeur(Noeud pRacine)
{
    Queue<Noeud> file = new Queue<Noeud>();
    file.Enqueue(pRacine);

    while (file.Count > 0)
    {
        Noeud tempo = file.Dequeue();
        Console.WriteLine(tempo.Valeur);
        for (int i = 0; i < tempo.Enfants.Count; i++)
            file.Enqueue(tempo.Enfants[i]);
    }
}
```

```c#
public List<Noeud> TrouverNoeudPlusGrand(Noeud pNoeud, int pValeur)
{
    List<Noeud> retour = new List<Noeud>();
    if(pNoeud.Valeur > pValeur)
        retour.Add(pNoeud);
    foreach (Noeud enfant in pNoeud.Enfants)
        retour.AddRange(TrouverNoeudPlusGrand(enfant, pValeur));
    return retour;

}
```


# Pratique examen

https://gitlab.com/progavance/contenu/-/blob/main/Ex3%20-%20A23.pdf?ref_type=heads
  
### Code Pratique examen

```c#
public void ChangementAffiliation(string pNomOrganisation, string pNomParent, bool pEnfantsSuivent)
{
    OrganisationSyndicale orgQuiChange = TrouverParNom(pNomOrganisation);
    if (!pEnfantsSuivent)
    {
        if (orgQuiChange.Parent is null)
        {
            foreach (OrganisationSyndicale enfants in orgQuiChange.Enfants)
            {
                enfants.Parent = null;
                lesOrganisationsRacines.Add(enfants);
            }
        }
        else
        {
            foreach (OrganisationSyndicale enfants in orgQuiChange.Enfants)
            {
                enfants.Parent = orgQuiChange.Parent;
                enfants.Parent.Enfants.Add(enfants);
            }
        }
        
        orgQuiChange.Enfants.Clear();
    }
    if (String.IsNullOrEmpty(pNomParent))
        lesOrganisationsRacines.Add(orgQuiChange);
    else
    {
        OrganisationSyndicale parent = TrouverParNom(pNomParent);
        parent.Enfants.Add(orgQuiChange);
        orgQuiChange.Parent = parent;
    }
}

public OrganisationSyndicale TrouverParNom(string pNom)
{
    if(Nom = pNom)
        return this;
    OrganisationSyndicale retour = null;
    foreach(OrganisationSyndicale enfant in Enfants)
    {        
        retour = enfant.TrouverParNom(pNom);
        if(retour is not null)
            return retour;
    }
    return null;
}
```
  
```c#
public double NegocierCommeLebel(double pOffre, double pOffreMaximale, List<string> pConditionsDeBouette, int pRondeNego)
{
    if (pOffre > pOffreMaximale)
        return 2;
    foreach (OrganisationSyndicale racine in lesOrganisationsRacines)
    {
        if (FaireUneOffreRidicule(pOffre, pConditionsDeBouette, racine))
            return pOffre;
    }
    pConditionsDeBouette.Add(AjouterUneConditionDeBouette());
    if (pRondeNego % 4 == 0)
        pOffre+=0.5;
    return NegocierCommeLebel(pOffre, pOffreMaximale, pConditionsDeBouette, pRondeNego+1);
}
```

```c#
public int CalculerDistance(OrganisationSyndicale pAutreOrganisation)
{
    int tempo = 0;
    if (Enfants.Contains(pAutreOrganisation))
        return 0;
    foreach (OrganisationSyndicale enf in Enfants) 
    {
        tempo = enf.CalculerDistance(pAutreOrganisation);
        if (tempo != -1)
            return tempo + 1;
    }
    return -1;
}
```

```c#
public List<OrganisationSyndicale> ClasserLesVraiesOrganisations()
{
    List<OrganisationSyndicale> retour = TrouverSansEnfants();
    TriBogo(retour);
    return retour;
}

public List<OrganisationSyndicale> TrouverSansEnfants()
{
    List<OrganisationSyndicale> retour = new List<OrganisationSyndicale>();
    if (Enfants.Count > 0)
        retour.Add(this);
    foreach (OrganisationSyndicale enfant in Enfants)
        retour.AddRange(enfant.TrouverSansEnfants());
    return retour;
}

public void TriBogo(List<OrganisationSyndicale> plist)
{
    while (!ListeEnOrdreDecroissant(plist))
        MelangerListe(plist);
}

public bool ListeEnOrdreDecroissant(List<OrganisationSyndicale> plist)
{
    bool retour = true;
    int i = 0;
    while (retour && i < plist.Count - 2)
        retour = plist[i].nbMembres >= plist[i + 1].nbMembres;
    return retour;
}

public void MelangerListe(List<OrganisationSyndicale> plist)
{
    List<OrganisationSyndicale> listeMelange = new List<OrganisationSyndicale>();
    int i = 0;
    while (plist.Count > 0) 
    {
        i = Random.Shared.Next(plist.Count);
        listeMelange.Add(plist[i]);
        plist.RemoveAt(i);
    }
    plist = listeMelange;
}
```


# Enigmes

1. Vous possédez une chèvre, un chou et un loup.  Vous devez les faire traverser une rivière et vous n’avez qu’une seule place dans votre chaloupe. Si la chèvre se retrouve seul avec le chou sur une rive elle le mange. Si le loup se retrouve seul avec la chèvre sur une rive il la mange. Comment traversez vous la rivière ?
   
2. Vous devez traverser un pont sans garde-corps durant une nuit sans lune.  Vous avez une torche qui vous permet de traverser le pont deux personnes à la fois, au rythme du plus lent bien sûr.  Sans torche il est impossible d’avancer.  Vous êtes 4 personnes en tout et voici le temps que chaque personne aura besoin pour traverser le pont :
   - Personne 1 : 6 min
   - Personne 2 : 8 min (il est un peu fatigué)
   - Personne 3 : 15 min (il s’est foulé la cheville en marchant dans le noir)
   - Personne 4 : 20 min (il aime beaucoup le PFK)
Votre torche restera allumée 50 min, comment faîtes-vous pour traverser le pont ?

3. Codez une méthode en C# qui solutionnerait le problème précédent.  La méthode reçoit en paramètre une liste d’integer qui représente le temps pour traverser le pont de chaque personne.  Cette méthode retourne une liste de string qui indique chacun des déplacements. Les strings en position impairs seront les allers et les strings en position paires seront les retours.  Faites que votre méthode puisse trouver la solution la plus optimale au problème pour n’importe quel nombre de personnes.

4. Vous avez 12 pierres entre les mains.  Elles ont tous le même poids sauf une qui est un peu différente que les autres, vous ne savez pas si elle plus lourde ou plus légère que les autres.  Vous avez une balance à deux plateaux qui n’affiche pas le poids. Comment faîtes-vous pour trouver la pierre qui a un poids différent et savoir si elle est plus légère ou plus lourde que les autres avec 3 pesées ?

5. Vous êtes dans un jeu télévisé et vous devez choisir d’ouvrir une porte parmi trois.  Deux portes s’ouvrent sur une pièce vide et la troisième s’ouvre sur une pièce remplie d’or.  Vous choisissez la porte 1, mais avant de l’ouvrir le présentateur ouvre la porte 2 qui révèle une pièce vide et vous offre de changer votre choix pour la porte 3. 
Est-ce que vous ouvrez la porte 1 ou bien vous changez d’idée pour la porte 3 ? Pourquoi ?

