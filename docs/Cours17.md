---
outline: deep
---

# Cours 17

# Objectifs
- Changement de paradigme vs les structures de données (utilisateur -> concepteur)
- Création de structures personnalisées
  
# Planification
<!-- no toc -->
- [PDPPE](#programme-de-développement-professionnel-du-personnel-enseignant)
- [Examen2]()
- [Nouveau pouvoir acquis](#concevoir-des-structures-de-données)
- [Multifile](#les-multifiles)
- [Code du cours](#code-du-cours)

# Programme de développement professionnel du personnel enseignant

- De kossé?
- Vidéo

# Examen 2

- Que voulez-vous comme contexte?
  
# Concevoir des structures de données

- Quand on utilise les structures de VS, on utilise le code fait par Microsoft
- Comme on est des développeurs, on a la capacité de créer nos propres structures de données
- Et oui, on est rendu à ce niveau de snobisme, peuh les outils que tu peux avoir en magasin sont tellement poches, que je fais mes propres outils.

# Les multifiles

- Malheureusement, les classes sociales existent encore et le concept de multifile et une réalité bien présente
  - Dans un terminal d'aéroport : file des premières classes et file des pauvres
  - Dans plusieurs événements : file des VIP et file des pauvres
  - Urgence : files d'attente en fonction du niveau de gravité
  - Livraison : file des colis/courrier express et file du courrier des pauvres
- Il n'y a pas la structure de données multifile dans VS ... créons-là!

### En code

- Wow, une classe générique!
- Wow, des structures de données imbriquées!

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/Multifile24.zip?ref_type=heads