---
outline: deep
---

# Cours 28
  
# Objectifs
- Création d'une structure arborescente personnalisée
  
# Planification
<!-- no toc -->
- [Arbre autobalancé](#arbre-binaire-autobalancé)
- [Code du cours](#code-du-cours)

# Arbre binaire autobalancé

- Un arbre binaire qui s'autobalance lors des ajouts/supressions
- Chacun des noeuds sera autobalancé
- Peut être balancé par le poids ou par la hauteur

### En code

- À partir du code du cours 25
- Création d'une classe ArbreBalancé qui contiendra la racine
- RotationGauche et RotationDroit
- AjoutBalancé

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/ArbreBinaireA24%20-%20Semibalanc%C3%A9.zip?ref_type=heads
