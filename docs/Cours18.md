---
outline: deep
---

# Cours 18

# Objectifs
- Changement de paradigme vs les structures de données (utilisateur -> concepteur)
- Création de structures personnalisées
  
# Planification
<!-- no toc -->
- [La commode magique de Jp](#la-commode-magique-de-jp)
- [Code du cours](#code-du-cours)

# La commode magique de Jp

- J'ai une commode magique qui a les propriétés suivantes
  - De la place infinie
  - Des tiroirs infinis
  - Des tiroirs nommés
  - Un accès direct aux tiroirs

# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/DictionnaireDePiles.zip?ref_type=heads