---
outline: deep
---

# Cours 22

# Objectifs
- Le dernier concept de la session : les arbres
  
# Planification
<!-- no toc -->
- [Les arbres](#les-arbres)
- [Exercices](#exercices)
- [Code du cours](#code-du-cours)

# Les arbres

- Structure de données un peu particulière, car elle est définie à l'intérieur de la classe par les relations entre les instances.
- Elle n'est pas créée à l'extérieur de la classe commes les autres (list<>, stack<>, ...)
- Tous les noeuds (sauf la racine) d'un arbre auront :
  - Un seul parent
  - 0-N enfants
- Le premier noeud d'un arbre est nommée racine. Il peut avoir 0-N enfants comme un noeud normal, mais il a la particularité de ne pas avoir de parent.
- Le noeud racine est le point d'entrée dans un arbre.
- Il est important de toujours conserver la référence au noeud racine, sans quoi on perd l'arbre au complet.

### En code

- À votre choix!
- Ajouter un noeud.
- Trouver un noeud.
- Supprimer un noeud.
- Déplacer noeud.

# En équipe

- Poids
- Hauteur

# Exercices

À partir de l'arbre suivant :
![Un arbre d'entiers](/images/arbreEntiers.png)  

Voici la classe des noeuds de l'arbre : 

```c#
public classe Noeud
{
   public int Valeur {get; set;}
   public Noeud Parent {get; set;}
   public List<Noeud> Enfants {get; set;}

   public Noeud(int pValeur)
   {
      Valeur = pValeur;
      Enfants = new List<Noeud>();
   }

   public void AjouterNoeud(Noeud pNoeud)
   {
      Enfants.Add(pNoeud);
      pNoeud.Parent = this;
   }
}
```
```c#
private Noeud racine;

private void ConstruireArbre()
{
    racine = new Noeud(12);

    Noeud n4 = new Noeud(4);
    Noeud n3 = new Noeud(3);
    Noeud n8 = new Noeud(8);
    n8.AjouterNoeud(n4);
    n8.AjouterNoeud(n3);
    racine.AjouterNoeud(n8);

    Noeud n10 = new Noeud(10);
    racine.AjouterNoeud(n10);

    Noeud n14 = new Noeud(14);
    Noeud n6 = new Noeud(6);
    Noeud n9 = new Noeud(9);
    n9.AjouterNoeud(n14);
    n9.AjouterNoeud(n6);

    Noeud n2 = new Noeud(2);
    Noeud n11 = new Noeud(11);
    Noeud n7 = new Noeud(7);
    n7.AjouterNoeud(n9);
    n7.AjouterNoeud(n2);
    n7.AjouterNoeud(n11);
    racine.AjouterNoeud(n7);
}
```

1. Écrivez une méthode non-récursive qui parcourt l’arbre en hauteur. La méthode doit visiter les nœuds dans cet ordre : 12-8-4-3-10-7-9-14-6-2-11
2. Écrivez une méthode récursive qui parcourt l’arbre en hauteur. La méthode doit visiter les nœuds dans cet ordre : 12-8-4-3-10-7-9-14-6-2-11
3. Écrivez une méthode qui parcourt l’arbre en largeur. La méthode doit visiter les nœuds dans cet ordre : 12-8-10-7-4-3-9-2-11-14-6
4. Écrivez une méthode NoeudsPlusGrand qui parcourt l’arbre et retourne tous les nœuds qui sont plus grand qu’un entier passé en paramètre.


# Code du cours

https://gitlab.com/progavance/contenu/-/blob/main/ArbresA24.zip?ref_type=heads